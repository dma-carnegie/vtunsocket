
///
/// @brief This file provides a CFSocket-like API for creating and using virtual tunnel sockets.
///
/// If the underlying native socket of a virtual CFSocket is needed, vsocket API (vsend/vrecv/etc...)
/// must be used to interact with it, since the underlying socket is a virtual tunnel socket.
///

#import <CoreFoundation/CoreFoundation.h>

// These functions are identical to the native CFSocket functions, and are defined for the sake
// of consistency with the rest of the VCFSocket functions.
#define VCFSocketGetNative( s )                                CFSocketGetNative ( s )
#define VCFSocketCreateRunLoopSource( allocator, s, order )    CFSocketCreateRunLoopSource ( allocator, s, order )
#define VCFSocketGetTypeID( void )                             CFSocketGetTypeID ( void )
#define VCFSocketGetSocketFlags( s )                           CFSocketGetSocketFlags ( s )
#define VCFSocketSetSocketFlags( s, flags )                    CFSocketSetSocketFlags ( s, flags )
#define VCFSocketDisableCallBacks( s, callBackTypes )          CFSocketDisableCallBacks ( s, callBackTypes )
#define VCFSocketEnableCallBacks( s, callBackTypes )           CFSocketEnableCallBacks ( s, callBackTypes )
#define VCFSocketInvalidate( s )                               CFSocketInvalidate ( s )
#define VCFSocketIsValid( s )                                  CFSocketIsValid ( s )

/// @brief The protocol number that can be passed as the protocol argument for VCFSocketCreate* calls
/// instead of passing 0 or IPPROTO_TCP to try to create aggregated stream socket.
/// Sockets created using this protocol cannot be converted back to regular TCP, and socket tags do not apply to them.
extern const SInt32 VSocketProtocolSingleStreamAggregation;

/// @brief Creates a virtual CFSocket object of a specified protocol and type.
/// @param [in] allocator The allocator to use to allocate memory for the new object.
/// Pass NULL or kCFAllocatorDefault to use the current default allocator.
/// @param [in] protocolFamily The protocol family for the socket. If negative or 0
/// is passed, the socket defaults to PF_INET.
/// @param [in] socketType The socket type to create. If protocolFamily is PF_INET
/// and socketType is negative or 0, the socket type defaults to SOCK_STREAM.
/// @param [in] protocol The protocol for the socket. If protocolFamily is PF_INET
/// and protocol is negative or 0, the socket protocol defaults to IPPROTO_TCP if
/// socketType is SOCK_STREAM or IPPROTO_UDP if socketType is SOCK_DGRAM.
/// @param [in] callBackTypes A bitwise-OR combination of the types of socket activity
/// that should cause callout to be called.
/// @param [in] callout The function to call when one of the activities indicated by callBackTypes occurs.
/// @param [in] context A structure holding contextual information for the CFSocket object.
/// @return The new CFSocket object, or NULL if an error occurred.
CFSocketRef VCFSocketCreate (
        CFAllocatorRef allocator, SInt32 protocolFamily, SInt32 socketType,
        SInt32 protocol, CFOptionFlags callBackTypes, CFSocketCallBack callout,
        const CFSocketContext * context );

/// @brief Creates a virtual CFSocket object and opens a connection to a remote socket.
/// @param [in] allocator The allocator to use to allocate memory for the new object.
/// Pass NULL or kCFAllocatorDefault to use the current default allocator.
/// @param [in] signature A CFSocketSignature identifying the communication protocol
/// and address to which the CFSocket object should connect.
/// @param [in] callBackTypes A bitwise-OR combination of the types of socket activity
/// that should cause callout to be called.
/// @param [in] callout The function to call when one of the activities indicated by callBackTypes occurs.
/// @param [in] context A structure holding contextual information for the CFSocket object.
/// @param [in] timeout The time (in seconds) to wait for a connection to succeed. If a negative value is
/// used, this function does not wait for the connection and instead lets the connection attempt
/// happen in the background. If callBackTypes includes kCFSocketConnectCallBack, you will receive
/// a callback when the background connection succeeds or fails.
/// @return The new CFSocket object, or NULL if an error occurred.
CFSocketRef VCFSocketCreateConnectedToSocketSignature (
        CFAllocatorRef allocator, const CFSocketSignature * signature,
        CFOptionFlags callBackTypes, CFSocketCallBack callout,
        const CFSocketContext * context, CFTimeInterval timeout );

/// @brief Creates a virtual CFSocket object using information from a CFSocketSignature structure.
/// @param [in] allocator The allocator to use to allocate memory for the new object.
/// @param [in] signature A CFSocketSignature identifying the communication protocol and
/// address with which to create the CFSocket object.
/// @param [in] callBackTypes A bitwise-OR combination of the types of socket activity
/// that should cause callout to be called.
/// @param [in] callout The function to call when one of the activities indicated by callBackTypes occurs.
/// @param [in] context A structure holding contextual information for the CFSocket object.
/// @return The new CFSocket object, or NULL if an error occurred.
CFSocketRef VCFSocketCreateWithSocketSignature (
        CFAllocatorRef allocator, const CFSocketSignature * signature,
        CFOptionFlags callBackTypes, CFSocketCallBack callout,
        const CFSocketContext * context );

/// @brief Returns the local address of a CFSocket object.
/// @param [in] s The CFSocket object to examine.
/// @return The local address of s, stored as a struct sockaddr appropriate for the CFSocket.
CFDataRef VCFSocketCopyAddress ( CFSocketRef s );

/// @brief Returns the remote address to which a CFSocket object is connected.
/// @param [in] s The CFSocket object to examine.
/// @return The remote address to which s is connected, stored as a struct sockaddr appropriate for the CFSocket.
CFDataRef VCFSocketCopyPeerAddress ( CFSocketRef s );

/// @brief Returns the context information for a CFSocket object.
/// @param [in] s The CFSocket object to examine.
/// @param [in] context A pointer to the structure into which the context information for s is to be copied.
void VCFSocketGetContext ( CFSocketRef s, CFSocketContext * context );

/// @brief Binds a local address to a CFSocket object and, if successfully bound, configures it for listening.
/// The listen backlog is set to 256.
/// @param [in] s The CFSocket object to modify.
/// @param [in] address A CFData object containing a struct sockaddr appropriate for the CFSocket.
/// @return On success: kCFSocketSuccess is returned.
///         On error: kCFSocketError is returned, and errno is set appropriately.
CFSocketError VCFSocketSetAddress ( CFSocketRef s, CFDataRef address );

/// @brief Opens a connection to a remote socket.
/// @param [in] s The CFSocket object with which to connect to address.
/// @param [in] address A CFData object containing a struct sockaddr indicating the remote address to which to connect.
/// @param [in] timeout The time (in seconds) to wait for a connection to succeed. If a
/// negative value is used, this function does not wait for the connection and instead
/// lets the connection attempt happen in the background. If s requested a kCFSocketConnectCallBack,
/// you will receive a callback when the background connection succeeds or fails.
/// @return On success: kCFSocketSuccess is returned.
///         On timeout: kCFSocketTimeout is returned.
///         On other errors: kCFSocketError is returned, and errno is set appropriately.
CFSocketError VCFSocketConnectToAddress ( CFSocketRef s, CFDataRef address, CFTimeInterval timeout );

/// @brief Sends data over a CFSocket object. This should be used instead of calling send on
/// the native socket FD.
/// @param [in] s The CFSocket object to use.
/// @param [in] address A CFData object containing a struct sockaddr indicating the destination address to which.
///                     For TCP sockets this is ignored.
///                     This is optional (can be NULL).
/// @param [in] data Buffer that stores the data to send. Cannot be NULL.
/// @param [in] timeout The max time (in seconds) to block waiting for the data to be sent.
/// @return On success: kCFSocketSuccess is returned.
///         On timeout: kCFSocketTimeout is returned.
///         On other errors: kCFSocketError is returned, and errno is set appropriately.
CFSocketError VCFSocketSendData ( CFSocketRef s, CFDataRef address, CFDataRef data, CFTimeInterval timeout );

/// @brief Get the value of a tag set on a virtual CFSocket.
/// Each data packet sent using this socket will be marked with that tag and, depending on internal configuration,
/// may be handled differently. Those tags are only used locally and never sent over the network.
/// They are arbitrary values whose meaning is not defined here, except for 0, which is the default tag value
/// assigned when no specific tag is configured on a socket.
/// @param [in] s The CFSocket object to get the tag from.
/// @param [out] tag Pointer to the variable to store the tag in. Cannot be 0.
/// @return On success: kCFSocketSuccess is returned.
///         On timeout: kCFSocketTimeout is returned.
///         On other errors: kCFSocketError is returned, and errno is set appropriately.
CFSocketError VCFSocketGetTag ( CFSocketRef s, int * tag );

/// @brief Set a tag on a virtual CFSocket.
/// Each data packet sent using this socket will be marked with that tag and, depending on internal configuration,
/// may be handled differently. Those tags are only used locally and never sent over the network.
/// They are arbitrary values whose meaning is not defined here, except for 0, which is the default tag value
/// assigned when no specific tag is configured on a socket.
/// @param [in] s The CFSocket object to set the tag on.
/// @param [in] tag The tag to set.
/// @return On success: kCFSocketSuccess is returned.
///         On timeout: kCFSocketTimeout is returned.
///         On other errors: kCFSocketError is returned, and errno is set appropriately.
CFSocketError VCFSocketSetTag ( CFSocketRef s, int tag );
