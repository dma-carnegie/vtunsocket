
///
/// @brief This file provides a BSD-like API for creating and using virtual tunnel sockets.
///
/// Then multiple sockets can be created with vsocket. This returns a regular local socket file descriptor
/// which can be used with poll/epoll/select, but not with the regular read/write/send/recv functions.
///
/// Instead the local socket fd can be used to communicate with the virtual tunnel daemon with the
/// functions provided in this header.
///

#pragma once

#ifndef PRAV_PUBLIC_API

#if ( defined( __unix__ ) || defined( __APPLE__ ) ) && ( defined( __GNUC__ ) || defined( __clang__ ) )
#define PRAV_PUBLIC_API    __attribute__( ( __visibility__ ( "default" ) ) )
#elif defined( WIN32 ) && defined( BUILDING_DLL )
#define PRAV_PUBLIC_API    __declspec ( dllexport )
#elif defined( WIN32 )
#define PRAV_PUBLIC_API    __declspec ( dllimport )
#else
#define PRAV_PUBLIC_API
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/socket.h>
#include <netinet/in.h>

struct ifaddrs;
struct addrinfo;

/// @def VSOCK_IPPROTO_AGGREGATION
/// The protocol number that can be passed as the third argument of vsocket* calls
/// instead of passing 0 or IPPROTO_TCP to try to create aggregated stream socket.
/// If aggregation is not supported, a regular stream socket will be created instead.
/// The number is from the end of IANA's unassigned range (143-252),
/// to reduce chances of any conflicts. 231 looks symmetric in binary...
/// Sockets created using this protocol cannot be converted back to regular TCP, and socket tags do not apply to them.
#define VSOCK_IPPROTO_AGGREGATION    231

/// @def VSOCK_CREATE_NATIVE
/// This flag makes vsocket2() create a native socket.
/// @note This will cause the timeout parameter and all the other flags to be ignored.
///       When this flag is passed, vsocket2() behaves exactly like the native socket() call.
#define VSOCK_CREATE_NATIVE    ( 1 << 0 )

/// @def VSOCK_CREATE_VIRTUAL
/// This flag forces vsocket2() to only create virtual sockets. If a virtual socket
/// cannot be created, vsocket2() will fail (instead of creating a native socket).
/// @note This flag has no effect when used together with VSOCK_CREATE_NATIVE flag.
#define VSOCK_CREATE_VIRTUAL    ( 1 << 1 )

/// @def VSOCK_SMART_TIMEOUT
/// This flag enables "smart timeout" mode.
/// It only does something when there is a positive timeout value passed to vsocket2() call.
/// In this mode, vsocket2() may choose to skip the timeout if virtual socket cannot be created right away.
/// The first time vsocket2() is used with a timeout, it will wait until failing or falling back to
/// native sockets. When that happens, an internal flag will be flipped, and next vsocket2() calls will
/// try to create virtual sockets, but will fail (or fall back to native sockets) right away.
/// The next time vsocket2() successfully generates a virtual socket, that flag will be flipped back,
/// and timeout will be respected again - until the next time it times out.
/// @note This behaviour is for waiting for MAS client to be able to generate valid virtual sockets.
///       When MAS client cannot be contacted at all, the timeout will be used, but only the first time.
///       After the first time waiting for MAS client times out or succeeds, vsocket2() will never wait
///       for it again.
#define VSOCK_SMART_TIMEOUT    ( 1 << 2 )

/// @def VSOCK_EXACT_ADDR_FAMILY
/// Normally the vsocket request will fail if the tunnel has no IP addresses assigned.
/// Even if the socket family requested doesn't match any of the addresses that are assigned to the tunnel,
/// vsockets will still be generated. This is because v6 sockets can later be used for IPv4 traffic.
/// Passing this flag will enable address type checking. This means that v6 sockets will only
/// be generated when the tunnel has at least one v6 address (and the same for v4 addresses).
#define VSOCK_EXACT_ADDR_FAMILY    ( 1 << 3 )

// The types of vsockets (used in the VSocketInfo.type field)
/// @def VSOCKET_TYPE_NATIVE
/// A native socket
#define VSOCKET_TYPE_NATIVE                0

/// @def VSOCKET_TYPE_VIRTUAL
/// A virtual socket
#define VSOCKET_TYPE_VIRTUAL               1

/// @def VSOCKET_TYPE_VIRTUAL_AGGREGATED
/// A virtual aggregated socket
#define VSOCKET_TYPE_VIRTUAL_AGGREGATED    2

/// @brief A struct that contains various parameters of a vsocket
struct VSocketInfo
{
    int type; ///< The type of vsocket, one of VSOCKET_TYPE_*
    int tag; ///< The virtual socket's tag
};

/// @brief Creates a new virtual tunnel socket file descriptor.
///
///        This socket is actually a local IPC socket used to communicate with the virtual tunnel daemon.
///
///        It is still a file descriptor that can be used with poll/epoll/select,
///        but not with the regular read/write/send/recv functions.
///
///        Also the file descriptor can be set to non-blocking mode if needed.
///
/// @note  These functions behave like regular BSD socket functions, except they can also fail
///        if the local IPC fails for any reason.
///
/// @param [in] family The socket family
/// @param [in] type The socket type
/// @param [in] protocol The socket protocol
/// @return On success: the file descriptor is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vsocket ( int family, int type, int protocol );

/// @brief Creates a new virtual tunnel socket file descriptor.
/// Just like vsocket, but this version takes additional flags and a timeout value.
/// @param [in] family The socket family
/// @param [in] type The socket type
/// @param [in] protocol The socket protocol
/// @param [in] flags A bit sum of VSOCK_* flags.
/// @param [in] timeout The time (in milliseconds) for which this function should wait until a virtual socket
///                      can be created, before falling back to a native socket (or failing, depending on the flags).
///                      If 0, this function returns right away.
///                      This timeout is only used after virtual tunnel API has been initialized with a correct path.
///                      If the API is not initialized properly, this function always returns right away.
///                      Note that this time may not be measured accurately.
/// @return On success: the file descriptor is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vsocket2 ( int family, int type, int protocol, unsigned int flags, unsigned int timeout );

/// @brief Creates a new virtual tunnel socket file descriptor.
/// @note This is a deprecated wrapper around the new call: vsocket2().
///       Calling this is equivalent to calling vsocket2() with the same timeout and VSOCK_SMART_TIMEOUT flag.
/// @param [in] family The socket family
/// @param [in] type The socket type
/// @param [in] protocol The socket protocol
/// @param [in] timeout The time (in milliseconds) for which this function should wait until a virtual socket
///                      can be created, before falling back to a native socket (or failing, depending on the flags).
///                      If 0, this function returns right away.
///                      This timeout is only used after virtual tunnel API has been initialized with a correct path.
///                      If the API is not initialized properly, this function always returns right away.
///                      Note that this time may not be measured accurately.
/// @return On success: the file descriptor is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vsocket_timed ( int family, int type, int protocol, unsigned int timeout );

/// @brief Closes a virtual tunnel socket; this is just the regular close function.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vclose ( int fd );

/// @brief Bind a virtual tunnel socket to the given local socket address.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] addr Pointer to the socket address
/// @param [in] addrLen Length of the socket address
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vbind ( int fd, const struct sockaddr * addr, socklen_t addrLen );

/// @brief Connect a virtual tunnel socket to the given remote socket address.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] addr Pointer to the socket address
/// @param [in] addrLen Length of the socket address
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vconnect ( int fd, const struct sockaddr * addr, socklen_t addrLen );

/// @brief Connect a virtual tunnel socket to the given remote socket address.
/// This version ignores the blocking/non-blocking mode of the socket, and it could block
/// even when the socket passed is in non-blocking mode.
/// The timeout parameter controls that behaviour.
/// Instead it uses timeout field to specify the behaviour.
/// @note If this call fails, the blocking mode of the socket (O_NONBLOCK flag) may have been modified.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] addr Pointer to the socket address
/// @param [in] addrLen Length of the socket address
/// @param [in] timeout The timeout in milliseconds.
///                     0 means that this works like regular non-blocking connect, and this call will return right away.
///                     < 0 means that this works like regular blocking connect, and this call will block
///                     until it succeeds or there is an error - even if the socket is in non-blocking mode.
///                     > 0 works like blocking connect, but the maximum amount of time this could block is limited
///                     by the given number of milliseconds. In case of a timeout, ETIMEDOUT error code is sent back.
///                     Note that this time may not be measured accurately.
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vconnect_timed ( int fd, const struct sockaddr * addr, socklen_t addrLen, int timeout );

/// @brief Get the local socket address that the virtual tunnel socket is bound to.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [out] addr Pointer to the buffer to write the socket address
/// @param [in,out] addrLen Pointer to the length of the socket address buffer.
///                         This length will be assigned the actual size of the address on success.
///                         If the buffer is too small to store the address, the address will be truncated,
///                         BUT addrLen will still be assigned the full size of the address.
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vgetsockname ( int fd, struct sockaddr * addr, socklen_t * addrLen );

/// @brief Get the remote socket address that the virtual tunnel socket is connected to.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [out] addr Pointer to the buffer to write the socket address
/// @param [in,out] addrLen Pointer to the length of the socket address buffer.
///                         This length will be assigned the actual size of the address on success.
///                         If the buffer is too small to store the address, the address will be truncated,
///                         BUT addrLen will still be assigned the full size of the address.
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vgetpeername ( int fd, struct sockaddr * addr, socklen_t * addrLen );

/// @brief Get the value of a socket option.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] level The protocol level of the option.
///                   To manipulate options at the sockets API level, level is specified as SOL_SOCKET.
///                   To manipulate options at any other level the protocol number of the appropriate protocol
///                   controlling the option should be supplied.
/// @param [in] optName The socket option to get
/// @param [out] optVal Pointer to the buffer to store the option value
/// @param [in,out] optLen Pointer to the length of the option value buffer.
///                        This length will be assigned the actual size of the option value on success.
///                        This length must be at least the size of the option value, otherwise it is an error.
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vgetsockopt ( int fd, int level, int optName, void * optVal, socklen_t * optLen );

/// @brief Set the value of a socket option.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] level The protocol level of the option.
///                   To manipulate options at the sockets API level, level is specified as SOL_SOCKET.
///                   To manipulate options at any other level the protocol number of the appropriate protocol
///                   controlling the option should be supplied.
/// @param [in] optName The socket option to set
/// @param [in] optVal Pointer to the option value
/// @param [in] optLen Length of the option value
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vsetsockopt ( int fd, int level, int optName, const void * optVal, socklen_t optLen );

/// @brief Get the value of a tag set on a socket.
/// Each data packet sent using this socket will be marked with that tag and, depending on internal configuration,
/// may be handled differently. Those tags are only used locally and never sent over the network.
/// They are arbitrary values whose meaning is not defined here, except for 0, which is the default tag value
/// assigned when no specific tag is configured on a socket.
/// @param [in] fd The virtual tunnel socket file descriptor.
/// @param [out] tag Pointer to the variable to store the tag in. Cannot be 0.
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vgetsocktag ( int fd, int * tag );

/// @brief Set a tag on a socket.
/// Each data packet sent using this socket will be marked with that tag and, depending on internal configuration,
/// may be handled differently. Those tags are only used locally and never sent over the network.
/// They are arbitrary values whose meaning is not defined here, except for 0, which is the default tag value
/// assigned when no specific tag is configured on a socket.
/// @param [in] fd The virtual tunnel socket file descriptor.
/// @param [in] tag The tag to set.
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vsetsocktag ( int fd, int tag );

/// @brief Creates a linked list of structures describing the underlying virtual tunnel interface.
/// This function works like getifaddrs, but the only field used in each entry is ifa_addr.
/// All other fields will be set to 0.
/// @param [out] ifap The generated list. Should be freed by the caller with vfreeifaddrs() when it's no longer needed.
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vgetifaddrs ( struct ifaddrs ** ifap );

/// @brief Frees the ifaddrs list.
/// @param [in] ifa The list to free.
PRAV_PUBLIC_API void vfreeifaddrs ( struct ifaddrs * ifa );

/// @brief Write data to the socket.
///
///        Identical to calling vsend with zero flags.
///
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] buf Pointer to the data to send
/// @param [in] len Length of the data to send
/// @return On success: the number of bytes sent is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API ssize_t vwrite ( int fd, const void * buf, size_t len );

/// @brief Send data over the socket to the connected remote address.
///
///        Identical to calling vsendto with a NULL dstAddr argument.
///
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] buf Pointer to the data to send
/// @param [in] len Length of the data to send
/// @param [in] flags Bitwise combination of options
/// @return On success: the number of bytes sent is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API ssize_t vsend ( int fd, const void * buf, size_t len, int flags );

/// @brief Send data over the socket to the given address.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] buf Pointer to the data to send
/// @param [in] len Length of the data to send
/// @param [in] flags Bitwise combination of options
/// @param [in] dstAddr Pointer to the socket address (optional)
///                     Only AF_INET and AF_INET6 are supported.
/// @param [in] addrLen Length of the socket address (optional)
///                     If dstAddr is non-null, addrLen must be at least the size of struct sockaddr_in,
///                     and at most the size of struct sockaddr_storage.
/// @paramn On success: the number of bytes sent is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API ssize_t vsendto (
        int fd, const void * buf, size_t len, int flags, const struct sockaddr * dstAddr, socklen_t addrLen );

/// @brief Send data over the socket.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] msg Pointer to the msghdr struct to send.
///                 Only AF_INET and AF_INET6 are supported.
///                 If msg_name is non-null, msg_namelen must be at least the size of struct sockaddr_in,
///                 and at most the size of struct sockaddr_storage.
/// @param [in] flags Bitwise combination of options
/// @paramn On success: the number of bytes sent is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API ssize_t vsendmsg ( int fd, const struct msghdr * msg, int flags );

/// @brief Read data from the socket.
///
///        Identical to calling vrecv with zero flags.
///
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] buf Pointer to the buffer to receive into
/// @param [in] len Length of the buffer to receive into
/// @return On success: the number of bytes received is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API ssize_t vread ( int fd, void * buf, size_t len );

/// @brief Receive data over the socket. Normally used only on a connected socket.
///
///        Identical to calling vrecvfrom with a NULL srcAddr argument.
///
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in] buf Pointer to the buffer to receive into
/// @param [in] len Length of the buffer to receive into
/// @param [in] flags Bitwise combination of options
/// @return On success: the number of bytes received is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API ssize_t vrecv ( int fd, void * buf, size_t len, int flags );

/// @brief Receive data over the socket.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [out] buf Pointer to the buffer to receive into
/// @param [in] len Length of the buffer to receive into
/// @param [in] flags Bitwise combination of options
/// @param [out] srcAddr Pointer to the buffer to write the src address (optional)
///                      For TCP sockets this is ignored.
/// @param [in,out] addrLen Pointer to the length of the src address buffer (optional)
///                         If srcAddr is non-null, addrLen must be at least the size of struct sockaddr.
///                         This length will be assigned the actual size of the address on success.
///                         If the buffer is too small to store the address, the address will be truncated,
///                         BUT addrLen will still be assigned the full size of the address.
///                         For TCP sockets: addrLen will be assigned zero.
/// @return On success: the number of bytes received is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API ssize_t vrecvfrom (
        int fd, void * buf, size_t len, int flags, struct sockaddr * srcAddr, socklen_t * addrLen );

/// @brief Receive data over the socket.
/// @param [in] fd The virtual tunnel socket file descriptor
/// @param [in,out] msg Pointer to the msghdr struct to receive into
///                     If msg_name is non-null, msg_namelen must be at least the size of struct sockaddr.
///                     For TCP sockets: msg_name is ignored and msg_namelen will be assigned zero.
/// @param [in] flags Bitwise combination of options
/// @return On success: the number of bytes received is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API ssize_t vrecvmsg ( int fd, struct msghdr * msg, int flags );

/// @brief An equivalent of getaddrinfo() call, that operates using virtual tunnel sockets.
/// See getaddrinfo() for more information.
/// @note Similarly to other v*() functions, it will fallback to the default getaddrinfo() call
///        if the call using vtunsocket fails.
/// @param [in] node Identifies the Internet host to look for. Could be NULL, but not at the same time as service.
/// @param [in] service Identifies the Internet service to look for. Could be NULL, but not at the same time as node.
/// @param [in] hints Specifies additional search criteria. Could be NULL.
/// @param [out] res The location where the head of the result's list will be stored. Cannot be NULL.
/// @return 0 on success, or one of the error codes.
///         It uses error codes defined by getaddrinfo(), but can also return additional error codes.
///         To get the error description, vgai_strerror() should be used.
PRAV_PUBLIC_API int vgetaddrinfo (
        const char * node, const char * service, const struct addrinfo * hints, struct addrinfo ** res );

/// @brief Used to free the list of addrinfo structures generated by vgetaddrinfo().
/// @param [in] res The head of the list of addrinfo structures to remove.
PRAV_PUBLIC_API void vfreeaddrinfo ( struct addrinfo * res );

/// @brief Returns a string description of error codes returned by vgetaddrinfo().
/// @param [in] error The error code whose description to return.
/// @return A string description of an error code returned by vgetaddrinfo().
PRAV_PUBLIC_API const char * vgai_strerror ( int error );

/// @brief Queries the virtual tunnel for the address of the DNS server to use.
/// @param [out] addrs Memory to store the server addresses at. Each generated entry will use sockaddr_in6 size.
///                    All sockaddr_in addresses in the array will be padded to use that size.
///                    This array should be deallocated by the caller using 'free()'.
/// @param [out] num_addrs The number of entries in the array generated. This function succeeds only when
///                         at least one address was received.
/// @param [in] timeout The time (in milliseconds) after which this function will give up even when
///                      the address is (still) unavailable. Note that this time is not measured accurately,
///                      this is simply the total number of milliseconds that we will sleep for between retries.
///                      If < 1 it will give up after first failure.
/// @return 0 on success, -1 on error (and sets errno).
PRAV_PUBLIC_API int vdns_get_addrs ( struct sockaddr_in6 ** addrs, size_t * num_addrs, unsigned int timeout );

/// @brief Get the VSocketInfo of a socket.
/// @param [in] vsockFd The virtual tunnel socket file descriptor.
/// @param [out] info Pointer to the variable to store the socket info. Cannot be 0.
/// @return On success: zero is returned.
///         On error: -1 is returned, and errno is set appropriately.
PRAV_PUBLIC_API int vgetsockinfo ( int vsockFd, struct VSocketInfo * info );

#ifdef __cplusplus
}
#endif
